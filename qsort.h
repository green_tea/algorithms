#pragma once
#include <random>

std::mt19937 gen;

template <typename T>
void sort(std::vector <T>& ar, const int l, const int r) {
    gen.seed((unsigned int)time(0));
    qsort(ar, l, r);
}

template <typename T>
void qsort(std::vector <T>& ar, const int l, const int r) {
    if (r < l) return;
    T left = l, right = r;
    T d = ar[gen() % (r - l + 1) + l];
    while (left <= right) {
        while (ar[left] < d) ++left;
        while (ar[right] > d) --right;
        if (left <= right) std::swap(ar[left++], ar[right--]);
    }
    qsort(ar, l, right);
    qsort(ar, left, r);
}