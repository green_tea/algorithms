#include "src/math/Math_algo.h"
#include "src/math/Matrix.h"
#include "src/math/Permutation.h"
#include <iostream>

int main() {
    Math_algo math_algo;
    std::vector <long long> ar(9);
    for (int i = 0; i < 9; ++i) {
        std::cin >> ar[i];
    }
    std::cout << math_algo.get_max_sum(ar);
}