#pragma once

#include <random>
#include <algorithm>

std::mt19937 gen;

template <typename T>
int nth_element(std::vector <T> ar, int k) {
    gen.seed((unsigned int)time(0));
    return find(ar, 0, ar.size() - 1, k);
}

template <typename T>
int find(std::vector <T>& ar, const int l, const int r, const int k) {
    if (r - l <= 1) {
        if (k == l + 1) return std::min(ar[l], ar[r]);
        else return std::max(ar[l], ar[r]);
    }
    int left = l, right = r;
    T p = ar[gen() % (r - l + 1) + l]; 
    while (left <= right)
    {
        while (ar[left] < p) ++left;
        while (ar[right] > p) --right;
        if (left <= right) std::swap(ar[left++], ar[right--]);
    }
    --left;
    ++right;
    if (left == right) --left;
    if (left + 1 >= k) return find(ar, l, left, k);
    else return find(ar, right, r, k);
}