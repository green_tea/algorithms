//
// Created by ice-tea on 08.10.18.
//

#include <gtest/gtest.h>

#include "Game_algo.h"

TEST(Retroanalisys, simple_test1) {
    graph my_graph{{1, 2, 3}, {}, {4, 5, 6, 7, 8}, {2, 8}, {}, {6}, {}, {}, {}};
    std::vector <int> terminals{4, 6, 8};
    Game_algo game_algo;
    std::vector <int> result = game_algo.retro_analyze(my_graph, terminals);
    std::vector <int> assert_result{-1, -1, 1, 1, 0, 1, 0, -1, 0};
    EXPECT_EQ(result, assert_result);
}

TEST(Retroanalisys, simple_test2) {
    graph my_graph{{1}, {2}, {3}, {4}, {5}, {6}, {0}};
    std::vector <int> terminals{};
    Game_algo game_algo;
    std::vector <int> result = game_algo.retro_analyze(my_graph, terminals);
    std::vector <int> assert_result{-1, -1, -1, -1, -1, -1, -1};
    EXPECT_EQ(result, assert_result);
}

TEST(Retroanalisys, simple_test3) {
    graph my_graph{{1}, {2}, {3}, {4}, {5}, {6}, {0}};
    std::vector <int> terminals{6};
    Game_algo game_algo;
    std::vector <int> result = game_algo.retro_analyze(my_graph, terminals);
    std::vector <int> assert_result{0, 1, 0, 1, 0, 1, 0};
    EXPECT_EQ(result, assert_result);
}

