//
// Created by ice-tea on 08.10.18.
//

#include <gtest/gtest.h>

#include "algo/math/math_algo.h"


TEST(Euler_tests, test1) {
    std::vector<int> answers = { 1, 1, 2, 2, 4, 2, 6, 4, 6, 4, 10, 4, 12, 6, 8 };
    std::vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
    std::vector<int> results;
    for (auto el : data) {
        results.push_back(fi(el));
    }
    ASSERT_EQ(answers, results);
}

TEST(dividers_tests, test1) {
    std::vector<int> answer = { 1, 2, 4, 5, 10, 20, 25, 50, 100 };
    std::vector<int> result = get_dividers(100);
    ASSERT_EQ(answer, result);
}

TEST(dividers_tests, test2) {
    std::vector<int> answer = { 1 };
    std::vector<int> result = get_dividers(1);
    ASSERT_EQ(answer, result);
}

TEST(dividers_tests, test3) {
    std::vector<long long> answer = { 1, 89, 3271, 5683, 291119, 505787, 744203, 18589093, 66234067, 1654429277, 2434288013, 4229305649, 216651633157, 376408202761, 13834058777879, 1231231231231231 };
    std::vector<long long> result = get_dividers<long long>(1231231231231231);
    ASSERT_EQ(answer, result);
}

TEST(binary_power_tests, power_test_1) {

    int data = 10;
    int p = 3;

    int result = power(data, p);

    ASSERT_EQ(1000, result);
}

TEST(binary_power_tests, power_test_2) {

    int data = -1;
    int p = 3;

    int result = power(data, p);
    ASSERT_EQ(-1, result);

    p = 4;
    result = power(data, p);
    ASSERT_EQ(1, result);
}

TEST(binary_power_tests, power_test_3) {

    int data = 100;
    int p = 0;

    int result = power(data, p);
    ASSERT_EQ(1, result);
}

TEST(binary_power_tests, power_test_4) {
    long long data = 10000000;
    long long p = 2;

    long long result = power(data, p);
    ASSERT_EQ(100000000000000, result);
}

TEST(binary_power_mod_tests, mod_power_test_1) {

    int data = 100;
    int p = 100;

    int result = power_mod(data, p, 1);
    ASSERT_EQ(0, result);
}

TEST(binary_power_mod_tests, mod_power_test_2) {

    int data = 100;
    int p = 100;

    int result = power_mod(data, p, 1000);
    ASSERT_EQ(0, result);
}

TEST(binary_power_tests, mod_power_test_3) {

    long long data = 1000000;
    long long p = 1000000000;

    long long result = power_mod(data, p, 1000000000000ll);
    ASSERT_EQ((p * 6) % 12, result);
}

TEST(eratosphen_tests, eratosphen_test_1) {
    int n = 1;

    std::vector<int> result = eratosphen(n);
    std::vector<int> answer = {};
    ASSERT_EQ(answer, result);
}

TEST(eratosphen_tests, eratosphen_test_2) {

    int n = 100;

    std::vector<int> result = eratosphen(n);
    std::vector<int> answer = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83,
                               89, 97};
    ASSERT_EQ(answer, result);
}

TEST(eratosphen_tests, eratosphen_test_3) {

    int n = 2;

    std::vector<int> result = eratosphen(n);
    std::vector<int> answer = { 2 };

    ASSERT_EQ(answer, result);
}

TEST(gcd_tests, gcd_test_1) {

    int a = 100;
    int b = 100;

    int result = gcd(a, b);
    ASSERT_EQ(100, result);
}

TEST(gcd_tests, gcd_test_2) {

    int a = 100;
    int b = 1;

    int result = gcd(a, b);
    ASSERT_EQ(1, result);
}

TEST(gcd_tests, gcd_test_3) {

    int a = 300;
    int b = 39;

    int result = gcd(a, b);
    ASSERT_EQ(3, result);
}

TEST(gcd_tests, gcd_test_4) {

    int a = 47;
    int b = 1000;

    int result = gcd(a, b);
    ASSERT_EQ(1, result);
}

TEST(gcd_tests, gcd_test_5) {

    long long a = 128375127365;
    long long b = 2198361827;

    long long result = gcd(a, b);
    ASSERT_EQ(1, result);
}

TEST(gcd_tests, gcd_test_6) {

    long long a = 12837512736;
    long long b = 219836182;

    long long result = gcd(a, b);
    ASSERT_EQ(2, result);
}