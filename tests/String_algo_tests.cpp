//
// Created by ice-tea on 08.10.18.
//

#include <gtest/gtest.h>

#include "String_algo.h"

TEST(z_function_tests, simple_test1) {
    String_algo string_algo;
    std::string str = "aaaaa";
    std::vector<int> result = string_algo.z_function(str);
    std::vector<int> assert_result{5, 4, 3, 2, 1};
    EXPECT_EQ(result, assert_result);
    str = "aaabaab";
    result = string_algo.z_function(str);
    assert_result = {7, 2, 1, 0, 2, 1, 0};
    EXPECT_EQ(result, assert_result);
    str = "abacaba";
    result = string_algo.z_function(str);
    assert_result = {7, 0, 1, 0, 3, 0, 1};
    EXPECT_EQ(result, assert_result);
}
