//
// Created by ice_tea on 24.11.18.
//

#include <gtest/gtest.h>
#include <structures/linerial_structures/persistent/persistent_stack.h>


TEST(persistent_tests, test1) {
    persistent_stack<int> stack;

    // check empty
    ASSERT_THROW(stack.top(0), std::invalid_argument);
    stack.push(10, 0);

    ASSERT_EQ(stack.top(1), 10);
    stack.push(9, 1);

    ASSERT_EQ(stack.top(1), 10);
    ASSERT_EQ(stack.top(2), 9);
    stack.pop(1);

    // check empty
    ASSERT_THROW(stack.top(3), std::invalid_argument);
    ASSERT_THROW(stack.top(4), std::invalid_argument);
    ASSERT_THROW(stack.pop(4), std::invalid_argument);
    ASSERT_THROW(stack.push(10, 4), std::invalid_argument);
    stack.push(20, 3);
    stack.push(50, 4);
    ASSERT_EQ(stack.pop(5), 50);
    ASSERT_EQ(stack.top(6), 20);
    stack.push(30, 0);
    ASSERT_EQ(stack.pop(7), 30);
    ASSERT_THROW(stack.top(8), std::invalid_argument);

}

TEST(persistent_stack, test2) {
    persistent_stack<int> stack;
    stack.push(100);
    ASSERT_EQ(stack.top(), 100);
    stack.push(200);
    ASSERT_EQ(stack.pop(), 200);
    ASSERT_EQ(stack.pop(), 100);
    ASSERT_THROW(stack.pop(), std::invalid_argument);
    ASSERT_THROW(stack.top(), std::invalid_argument);
}

