//
// Created by ice-tea on 10.10.18.
//

#include <gtest/gtest.h>

#include <math/Matrix.h>

TEST(construct_tests, matrix_test1) {
    matrix mat = {{1, 2, 3}, {2, 3, 4}, {5, 6, 7}};
    Matrix matr(mat);
    EXPECT_EQ(matr.get_n(), 3);
    EXPECT_EQ(matr.get_m(), 3);
    EXPECT_EQ(matr.get_element(1, 1), 3);
}

TEST(construct_tests, matrix_test2) {
    Matrix mat(10, 20);
    EXPECT_EQ(mat.get_m(), 20);
    EXPECT_EQ(mat.get_n(), 10);
    EXPECT_EQ(mat.get_element(0, 0), 0);
}

TEST(construct_tests, matrix_test3) {
    Matrix mat;
    EXPECT_EQ(mat.get_n(), 0);
    EXPECT_EQ(mat.get_m(), 0);
}

TEST(construct_tests, copy_construct) {
    Matrix mat(10, 20);
    Matrix mat2(mat);
    EXPECT_EQ(mat2.get_n(), 10);
    mat.set_element(1, 1, 10);
    EXPECT_EQ(mat.get_element(1, 1), 10);
    EXPECT_EQ(mat2.get_element(1, 1), 0);
}

TEST(construct_tests, eq_test) {
    Matrix mat(10, 20);
    Matrix mat2;
    mat2 = mat;
    EXPECT_EQ(mat2.get_n(), 10);
    EXPECT_EQ(mat2.get_m(), 20);
    mat.set_element(1, 1, 10);
    EXPECT_EQ(mat2.get_element(1, 1), 0);
    mat2 = mat;
    EXPECT_EQ(mat2.get_element(1, 1), 10);
    EXPECT_EQ(mat.get_element(1, 1), 10);
}

TEST(static_functions, E_test) {
    Matrix mat(Matrix::get_E(3));
    EXPECT_EQ(mat.get_n(), 3);
    EXPECT_EQ(mat.get_m(), 3);
    matrix m {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    EXPECT_EQ(mat.get_data(), m);
}

TEST(static_functions, zero_test) {
    Matrix mat(Matrix::get_zero(3));
    matrix m {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
    EXPECT_EQ(mat.get_data(), m);
}

TEST(operation_tests, one_arity_minus) {
    Matrix mat(Matrix::get_E(3));
    mat = -mat;
    matrix m {{-1, 0, 0}, {0, -1, 0}, {0, 0, -1}};
    EXPECT_EQ(mat.get_data(), m);
}

TEST(operation_tests, diff_and_sum_test) {
    Matrix mat(Matrix::get_E(3));
    mat = mat + mat;
    matrix m {{2, 0, 0}, {0, 2, 0}, {0, 0, 2}};
    EXPECT_EQ(mat.get_data(), m);
    mat = mat - mat;
    EXPECT_EQ(mat.get_data(), Matrix::get_zero(3).get_data());
    EXPECT_THROW(mat + Matrix::get_E(5), std::invalid_argument);
}

TEST(operation_tests, mul_test) {
    Matrix mat({{1, 2}, {3, 4}});
    mat = mat * mat;
    matrix m {{7, 10}, {15, 22}};
    EXPECT_EQ(mat.get_data(), m);
    EXPECT_EQ((mat * Matrix::get_zero(2)).get_data(), Matrix::get_zero(2).get_data());
    EXPECT_EQ((mat * Matrix::get_E(2)).get_data(), mat.get_data());
    Matrix mat2(2, 1);
    Matrix res_mat = mat * mat2;
    EXPECT_EQ(res_mat.get_n(), 2);
    EXPECT_EQ(res_mat.get_m(), 1);
    EXPECT_THROW(mat2 * mat, std::invalid_argument);
}

TEST(operation_tests, mult_by_mod_test) {
    Matrix mat({{1, 2}, {3, 4}});
    mat = Matrix::mult_by_mod(mat, mat, 10);
    matrix m {{7, 0}, {5, 2}};
    EXPECT_EQ(mat.get_data(), m);
}

TEST(other_tests, resize_test) {
    Matrix mat(10, 3);
    mat.resize(15, 20);
    EXPECT_EQ(mat.get_n(), 15);
    EXPECT_EQ(mat.get_m(), 20);
}

TEST(other_tests, is_square_test) {
    Matrix mat(10, 10);
    EXPECT_TRUE(mat.is_square());
    mat.resize(10, 15);
    EXPECT_FALSE(mat.is_square());
}

TEST(other_test, get_modulo) {
    Matrix mat({{10, 20}, {30, 40}});
    Matrix res_mat = mat.get_modulo(20);
    matrix m {{10, 0}, {10, 0}};
    EXPECT_EQ(res_mat.get_data(), m);
}

TEST(other_test, failed_tests) {
    EXPECT_THROW(Matrix mat(-20, 13), std::exception);
    Matrix mat(10, 20);
    EXPECT_THROW(mat.resize(-20, 30), std::exception);
    EXPECT_THROW(mat.resize(-30, 30), std::exception);
}