//
// Created by ice-tea on 08.10.18.
//

#include <gtest/gtest.h>
#include <structures/linerial_structures/arrays/hashed_array_tree.h>

TEST(hashed_array_tree, test1) {
    hashed_array_tree<int> array_tree;

    array_tree.push(10);
    EXPECT_EQ(10, array_tree.top());
    array_tree.push(20);
    EXPECT_EQ(20, array_tree.top());
    EXPECT_EQ(2, array_tree.size());
    for (int i = 1; i < 101; ++i) {
        array_tree.push(i);
    }
    EXPECT_EQ(100, array_tree.top());
    EXPECT_EQ(102, array_tree.size());
    EXPECT_EQ(20, array_tree[1]);
    EXPECT_EQ(10, array_tree[0]);
    EXPECT_EQ(1, array_tree[2]);
    EXPECT_EQ(100, array_tree[101]);
    EXPECT_EQ(10, array_tree.at(0));
    EXPECT_EQ(100, array_tree[101]);
    EXPECT_EQ(54, array_tree[55]);
    EXPECT_EQ(28, array_tree[29]);
    EXPECT_EQ(72, array_tree[73]);
    EXPECT_THROW(array_tree.at(102), std::invalid_argument);
    for (int i = 0; i < 101; ++i) {
        array_tree.pop();
    }
    EXPECT_EQ(10, array_tree.top());
    array_tree.pop();
    EXPECT_THROW(array_tree.top(), std::invalid_argument);
    EXPECT_THROW(array_tree.pop(), std::invalid_argument);
}