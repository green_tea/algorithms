//
// Created by ice-tea on 08.10.18.
//

#include <gtest/gtest.h>

#include "algo/rest/rest_algo.h"

TEST(segment_sum_tests, test1) {
    using namespace rest_algo;

    std::vector<int> array = {1, 2, 3, -10, 14, 2, 7, -10, 13, 2, -7, 1};
    int result = get_max_sum_on_segment(array);
    ASSERT_EQ(28, result);

    std::vector<long long> second_array = {1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000};
    long long second_result = get_max_sum_on_segment<long long>(second_array);
    ASSERT_EQ(7000000000000, second_result);
}

TEST(segment_sum_tests, test2) {
    using namespace rest_algo;

    std::vector<int> array = {};
    int result = get_max_sum_on_segment(array);
    ASSERT_EQ(0, result);

    std::vector<long long> second_array = {-1, -2, -3, -4, -5};
    long long second_result = get_max_sum_on_segment<long long>(second_array);
    ASSERT_EQ(0, second_result);

    std::vector<int> third_array = {-1, -2, -3, -4, 1, -1, -2, -3};
    int third_result = get_max_sum_on_segment(third_array);
    ASSERT_EQ(1, third_result);
}


TEST(segment_sum_tests, test3) {
    using namespace rest_algo;

    std::vector<int> array = {1, 2, 3, -10, 14, 2, 7, -10, 13, 2, -7, 1};
    std::tuple<int, std::size_t, std::size_t> result = get_max_sum_segment(array);

    ASSERT_EQ(std::make_tuple(28, 4, 9), result);

    std::vector<long long> second_array = {1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000, 1000000000000};
    std::tuple<long long, std::size_t, std::size_t> second_result = get_max_sum_segment<long long>(second_array);
    ASSERT_EQ(std::make_tuple(7000000000000, 0, 6), second_result);
}

TEST(segment_sum_tests, test4) {
    using namespace rest_algo;

    std::vector<int> array = {};
    std::tuple<int, std::size_t, std::size_t> result = get_max_sum_segment(array);
    ASSERT_EQ(std::make_tuple(0, 0, 0), result);

    std::vector<long long> second_array = {-1, -2, -3, -4, -5};
    std::tuple<long long, std::size_t, std::size_t> second_result = get_max_sum_segment<long long>(second_array);
    ASSERT_EQ(std::make_tuple(0, 5, 5), second_result);

    std::vector<int> third_array = {-1, -2, -3, -4, 1, -1, -2, -3};
    std::tuple<int, std::size_t, std::size_t> third_result = get_max_sum_segment(third_array);
    ASSERT_EQ(std::make_tuple(1, 4, 4), third_result);
}

