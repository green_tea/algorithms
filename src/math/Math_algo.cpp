//
// Created by ice-tea on 26.09.18.
//

#include "Math_algo.h"

long long Math_algo::pow(long long number, long long int step) {
    return pow_by_mod(number, step, LLONG_MAX);
}

long long Math_algo::get_fibo_by_mod(long long x, long long modulo) {
    Matrix m({ { 0, 1 }, { 1, 1 } });
    return matrix_pow_by_mod(m, x, modulo).get_element(1, 0);
}

Matrix Math_algo::matrix_pow_by_mod(const Matrix& m, long long step, long long modulo) {
    if (!m.is_square()) throw std::invalid_argument("В степень можно возводить только квадратную матрицу.");
    if (step == 0) return Matrix::get_E(m.get_n());
    if (step == 1) return m.get_modulo(modulo);
    Matrix k = matrix_pow_by_mod(m, step / 2, modulo);
    return Matrix::mult_by_mod(Matrix::mult_by_mod(k, k, modulo), matrix_pow_by_mod(m, step % 2, modulo), modulo);
}

long long Math_algo::pow_by_mod(long long number, long long step, long long modulo) {
    if (step == 0) return 1;
    if (step == 1) return number % modulo;
    long long k = pow_by_mod(number, step / 2, modulo) % modulo;
    return ((k * k) % modulo * pow_by_mod(number, step % 2, modulo)) % modulo;
}

Matrix Math_algo::matrix_pow(const Matrix &m, long long step) {
    return matrix_pow_by_mod(m, step, LLONG_MAX);
}

