//
// Created by ice-tea on 26.09.18.
//

#ifndef ALGO_PERMUTATION_H
#define ALGO_PERMUTATION_H


#include <cstddef>
#include <vector>
#include <exception>
#include <iostream>

class Permutation {
public:
    explicit Permutation(const std::vector<int>& input);

    Permutation();

    explicit Permutation(size_t size);

    friend Permutation operator*(const Permutation& lhs, const Permutation& rhs);

    int&operator[](size_t index);

    const int&operator[](size_t index) const;

    Permutation&operator=(const Permutation& rhs);

    Permutation(const Permutation& permutation);

    size_t get_size() const;

    friend std::istream&operator>>(std::istream& in, Permutation& permutation);

    friend std::ostream&operator<<(std::ostream& out, const Permutation& permutation);

private:
    size_t size;
    std::vector <int> data;

    void copy(const Permutation& permutation);

    void checkBound(size_t index) const;
};


#endif //ALGO_PERMUTATION_H
