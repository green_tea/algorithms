//
// Created by ice-tea on 26.09.18.
//

#include <stdexcept>
#include "Permutation.h"

Permutation::Permutation(const std::vector<int>& input) {
    size = input.size();
    data.resize(size);
    for (size_t i = 0; i < size; ++i) {
        data[i] = input[i];
    }
}

Permutation::Permutation(size_t size) {
    this->size = size;
    data.resize(size);
    for (size_t i = 0; i < size; ++i) {
        data[i] = (int)(i + 1);
    }
}

Permutation operator*(const Permutation &lhs, const Permutation &rhs) {
    if (lhs.size != rhs.size) throw std::invalid_argument("Размеры перестановок должны иметь одинаковый размер");
    Permutation answer(lhs.size);
    for (int i = 0; i < lhs.size; ++i) {
        answer[rhs[i]] = lhs[i];
    }
    return answer;
}

int &Permutation::operator[](size_t index) {
    checkBound(index);
    return data[index];
}

const int &Permutation::operator[](size_t index) const {
    checkBound(index);
    return data[index];
}

Permutation &Permutation::operator=(const Permutation &rhs) {
    copy(rhs);
    return *this;
}

Permutation::Permutation(const Permutation &permutation) {
    copy(permutation);
}

size_t Permutation::get_size() const {
    return size;
}

void Permutation::copy(const Permutation &permutation) {
    size = permutation.size;
    data.resize(size);
    for (size_t i = 0; i < size; ++i) {
        data[i] = permutation.data[i];
    }
}

void Permutation::checkBound(size_t index) const {
    if (index >= size) throw std::out_of_range("Индекс вне границ массива");
}

std::istream &operator>>(std::istream &in, Permutation &permutation) {
    in >> permutation.size;
    permutation.data.resize(permutation.size);
    for (auto& i : permutation.data) {
        in >> i;
    }
    return in;
}

std::ostream &operator<<(std::ostream &out, const Permutation &permutation) {
    out << permutation.size << "\n";
    for (auto i : permutation.data) {
        out << i << " ";
    }
    out << "\n";
    return out;
}

Permutation::Permutation() {
    size = 0;
};
