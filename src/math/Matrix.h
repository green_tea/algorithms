//
// Created by ice-tea on 26.09.18.
//

#ifndef ALGO_MATRIX_H
#define ALGO_MATRIX_H

#include <vector>
#include <iostream>
#include <exception>
#include <climits>

typedef std::vector < std::vector <long long> > matrix;

class Matrix {
public:

    explicit Matrix(const matrix& input);

    Matrix(std::size_t n, std::size_t m);

    Matrix();

    Matrix(const Matrix& m);

    Matrix&operator=(const Matrix& rhs);

    static Matrix get_E(size_t size);

    static Matrix get_zero(size_t size);

    Matrix operator-() const;

    friend Matrix operator*(const Matrix& lhs, const Matrix& rhs);

    friend Matrix operator-(const Matrix& lhs, const Matrix& rhs);

    friend Matrix operator+(const Matrix& lhs, const Matrix& rhs);

    static Matrix mult_by_mod(const Matrix& lhs, const Matrix& rhs, long long modulo);

    std::size_t get_n() const;

    std::size_t get_m() const;

    long long get_element(std::size_t i, std::size_t j) const;

    void set_element(std::size_t i, std::size_t j, long long element);

    friend std::istream&operator>>(std::istream& in, Matrix& mat);

    friend std::ostream&operator<<(std::ostream& out, const Matrix& mat);

    void resize(size_t n, size_t m);

    bool is_square() const;

    Matrix get_modulo(long long modulo) const;

    const matrix& get_data() const;

private:

    void copy(const Matrix& matrix);

    matrix data;

    std::size_t n, m;
};


#endif //ALGO_MATRIX_H
