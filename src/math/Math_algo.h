//
// Created by ice-tea on 26.09.18.
//

#ifndef ALGO_MATH_ALGO_H
#define ALGO_MATH_ALGO_H


#include <vector>
#include <algorithm>
#include "Matrix.h"
#include <climits>

class Math_algo {
public:

    long long pow(long long number, long long step);

    long long pow_by_mod(long long number, long long step, long long modulo);

    Matrix matrix_pow(const Matrix& m, long long step);

    Matrix matrix_pow_by_mod(const Matrix& m, long long step, long long modulo);

    long long get_fibo_by_mod(long long x, long long modulo);


private:

};


#endif //ALGO_MATH_ALGO_H
