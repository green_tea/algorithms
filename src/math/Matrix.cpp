//
// Created by ice-tea on 26.09.18.
//

#include <stdexcept>
#include "Matrix.h"

Matrix::Matrix(const matrix& input) {
    resize(input.size(), input[0].size());
    for (size_t i = 0; i < input.size(); ++i) {
        for (size_t j = 0; j < input[i].size(); ++j) {
            data[i][j] = input[i][j];
        }
    }
}

Matrix Matrix::get_E(size_t size) {
    Matrix answer(size, size);
    for (size_t i = 0; i < size; ++i) {
        answer.set_element(i, i, 1);
    }
    return answer;
}

Matrix Matrix::get_zero(size_t size) {
    return Matrix(size, size);
}

Matrix operator*(const Matrix &lhs, const Matrix &rhs) {
    return Matrix::mult_by_mod(lhs, rhs, LLONG_MAX);
}

Matrix::Matrix(std::size_t n, std::size_t m) {
    resize(n, m);
}

Matrix::Matrix() : n(0), m(0) {}

Matrix::Matrix(const Matrix &m) {
    copy(m);
}

Matrix &Matrix::operator=(const Matrix &rhs) {
    copy(rhs);
    return *this;
}

Matrix operator-(const Matrix &lhs, const Matrix &rhs) {
    if ((lhs.n != rhs.n) || (lhs.m != rhs.m)) throw std::invalid_argument("матрицы с заданными размерами нельзя вычитать");
    return lhs + (-rhs);
}

Matrix Matrix::operator-() const {
    Matrix answer(n, m);
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            answer.set_element(i, j, -get_element(i, j));
        }
    }
    return answer;
}

Matrix operator+(const Matrix &lhs, const Matrix &rhs) {
    if ((lhs.n != rhs.n) || (lhs.m != rhs.m)) throw std::invalid_argument("матрицы с заданными размерами нельзя складывать");
    Matrix answer(lhs.n, lhs.m);
    for (size_t i = 0; i < lhs.n; ++i) {
        for (size_t j = 0; j < lhs.m; ++j) {
            long long cur = lhs.get_element(i, j) + rhs.get_element(i, j);
            answer.set_element(i, j, cur);
        }
    }
    return answer;
}

std::size_t Matrix::get_n() const {
    return n;
}

std::size_t Matrix::get_m() const {
    return m;
}

long long Matrix::get_element(std::size_t i, std::size_t j) const {
    return data[i][j];
}

void Matrix::set_element(std::size_t i, std::size_t j, long long element) {
    data[i][j] = element;
}

void Matrix::copy(const Matrix &mat) {
    resize(mat.n, mat.m);
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            data[i][j] = mat.get_element(i, j);
        }
    }
}

std::istream &operator>>(std::istream & in, Matrix &mat) {
    in >> mat.n;
    in >> mat.m;
    for (size_t i = 0; i < mat.n; ++i) {
        for (size_t j = 0; j < mat.m; ++j) {
            in >> mat.data[i][j];
        }
    }
    return in;
}

std::ostream &operator<<(std::ostream &out, const Matrix &mat) {
    out << mat.n << " ";
    out << mat.m << "\n";
    for (size_t i = 0; i < mat.n; ++i) {
        for (size_t j = 0; j < mat.m; ++j) {
            out << mat.get_element(i, j) << " ";
        }
        out << "\n";
    }
    return out;
}

void Matrix::resize(size_t n, size_t m) {
    data.resize(n);
    for (auto& i : data) {
        i.resize(m);
    }
    this->n = n;
    this->m = m;
}

bool Matrix::is_square() const {
    return n == m;
}

Matrix Matrix::mult_by_mod(const Matrix &lhs, const Matrix &rhs, long long modulo) {
    if (lhs.m != rhs.n) throw std::invalid_argument("Матрицы заданных размеров нельзя перемножить");
    Matrix answer(lhs.n, rhs.m);
    for (size_t i = 0; i < lhs.n; ++i) {
        for (size_t j = 0; j < rhs.m; ++j) {
            long long sum = 0;
            for (size_t k = 0; k < lhs.m; ++k) {
                sum = (lhs.get_element(i, k) * rhs.get_element(k, j) % modulo + sum) % modulo;
            }
            answer.set_element(i, j, sum);
        }
    }
    return answer;
}

Matrix Matrix::get_modulo(long long modulo) const {
    Matrix answer(n, m);
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < m; ++j) {
            answer.set_element(i, j, get_element(i, j) % modulo);
        }
    }
    return  answer;
}

const matrix& Matrix::get_data() const {
    return data;
}


