//
// Created by ice-tea on 07.09.18.
//

#ifndef ALGO_GAME_ALGO_H
#define ALGO_GAME_ALGO_H

#endif //ALGO_GAME_ALGO_H

#include "../Config.h"

class Game_algo {
private:
    void dfs(int vertex, const graph& input_graph, vector<int>& answers, vector <int>& degrees);
public:
    vector <int> retro_analyze(const graph& input_graph, const vector<int>& terminals);
};
