//
// Created by ice-tea on 07.09.18.
//

#include "String_algo.h"

vector <int> String_algo::z_function(string str) {
    vector <int> answer(str.size());
    for (int i = 0, l = 0, r = 0; i < str.size(); ++i) {
        if (i <= r) {
            answer[i] = std::min(answer[i - l], r - i + 1);
        }
        while ((answer[i] + i < str.size()) && (str[answer[i]] == str[i + answer[i]])) ++answer[i];
        if (answer[i] + i - 1 > r) {
            r = answer[i] + i - 1;
            l = i;
        }
    }
    return answer;
}