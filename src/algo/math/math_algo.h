//
// Created by ice_tea on 30.11.18.
//

#ifndef ALGO_MATH_ALGO_H
#define ALGO_MATH_ALGO_H

#endif //ALGO_MATH_ALGO_H

#include <vector>
#include <algorithm>


template <typename T = int>
std::vector<std::pair<T, T> > fi_factorization(T value) {

    std::vector<std::pair<T, T> > ans;

    for (int i = 2; i * i <= value; ++i) {

    T cur_ans = 1;

    while (value % i == 0) {
    cur_ans *= i;
    value /= i;
    }

    ans.emplace_back(std::make_pair(cur_ans, cur_ans / i));

    }

    if (value > 1) ans.emplace_back(std::make_pair(value, 1));
    return ans;
};

template <typename T = int>
T fi(T x) {
    std::vector<std::pair<T, T> > fact = fi_factorization<T>(x);
    T answer = 1;
    for (auto &i : fact) {
        answer *= i.first - i.second;
    }
    return answer;
}

template <typename T = int>
std::vector<T> get_dividers(T value) {
    std::vector<T> ans;

    for (T i = 1; i * i <= value; ++i) {
        if (value % i == 0) {
            ans.push_back(i);
            if (i * i != value) ans.push_back(value / i);
        }
    }

    std::sort(ans.begin(), ans.end());
    return ans;
}

template <typename T = int>
T power(T number, T pow) {
    if (pow == 0)
        return 1;
    if (pow == 1)
        return number;
    T k = power(number, pow / 2);
    return k * k * power(number, pow % 2);
}

template <typename T = int>
T power_mod(T number, T pow, T m) {
    if (pow == 0)
        return 1 % m;
    if (pow == 1)
        return number % m;
    T k = power_mod(number, pow / 2, m);
    return ((k * k) % m * power_mod(number, pow % 2, m)) % m;
}

std::vector<int> eratosphen(int n) {

    auto used = new bool[n + 1];
    for (int i = 0; i <= n; ++i) {
        used[i] = false;
    }

    std::vector<int> answer(0);

    for (int i = 2; i <= n; ++i) {
        if (!used[i])
            answer.push_back(i);

        for (int j = i; j <= n; j += i)
            used[j] = true;
    }

    return answer;
}

template <typename T = int>
T gcd(T a, T b) {
    if (a == 0)
        return b;
    return gcd(b % a, a);
}