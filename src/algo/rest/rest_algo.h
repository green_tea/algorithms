//
// Created by ice_tea on 25.11.18.
//

namespace rest_algo {

#include <vector>
#include <tuple>

    /// get max sum on segment without boundaries
    template <typename T = int>
    T get_max_sum_on_segment(std::vector<T> array) {
        T cur_sum = 0, max = 0;
        for (auto el : array) {
            cur_sum += el;
            cur_sum = cur_sum < 0 ? 0 : cur_sum;
            max = cur_sum > max ? cur_sum : max;
        }
        return max;
    }

    /// get max sum on segment with boundaries
    template <typename T = int>
    std::tuple<T, std::size_t, std::size_t> get_max_sum_segment(std::vector<T> array) {
        T cur_sum = 0, max = 0;
        std::size_t max_l = array.size(), max_r = array.size();
        std::size_t cur_l = 0;
        for (std::size_t i = 0; i < array.size(); ++i) {
            cur_sum += array[i];
            if (cur_sum < 0) {
                cur_l = i + 1;
                cur_sum = 0;
            }
            if (cur_sum > max) {
                max = cur_sum;
                max_l = cur_l;
                max_r = i;
            }
        }
        return std::make_tuple(max, max_l, max_r);
    };
}
