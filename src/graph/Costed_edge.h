//
// Created by root on 22.10.18.
//

#ifndef ALGO_COSTED_EDGE_H
#define ALGO_COSTED_EDGE_H

#include "Edge.h"

template <typename T>
class Costed_edge : public Edge {
public:
    Costed_edge(size_t from, size_t to, T cost);

    Costed_edge&operator=(const Costed_edge& costed_edge);

    T get_cost();
private:

    T cost;
};


#endif //ALGO_COSTED_EDGE_H
