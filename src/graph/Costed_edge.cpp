//
// Created by root on 22.10.18.
//

#include "Costed_edge.h"

template<typename T>
Costed_edge<T>::Costed_edge(size_t from, size_t to, T cost) : Edge(from, to) {
    this->cost = cost;
}

template<typename T>
Costed_edge&Costed_edge<T>::operator=(const Costed_edge &costed_edge) {
    Edge(costed_edge.from_number, costed_edge.to_number);
    cost = costed_edge.cost;
}

template<typename T>
T Costed_edge<T>::get_cost() {
    return cost;
}

