//
// Created by root on 22.10.18.
//

#ifndef ALGO_EDGE_H
#define ALGO_EDGE_H

#include "../../Config.h"

class Edge {
public:
    Edge(size_t from, size_t to);

    Edge&operator=(const Edge& edge);

    size_t from();

    size_t to();

protected:
    size_t from_number, to_number;

};


#endif //ALGO_EDGE_H
