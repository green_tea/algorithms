//
// Created by root on 22.10.18.
//

#include "Graph.h"

Graph::Graph(size_t size) {
    this->size = size;
    graph.resize(size);
    used = new bool[size];
}

Graph &Graph::operator=(const Graph &rhs) {
    size = rhs.size;
    graph = rhs.graph;
    for (int i = 0; i < size; ++i) {
        used[i] = rhs.used[i];
    }
    return *this;
}

Graph::~Graph() {
    delete [] used;
}

template<class T>
bool Graph<T>::is_used(size_t vertex_num) {
    if (vertex_num > size) throw std::invalid_argument("index out of range");
    return used[vertex_num];
}
