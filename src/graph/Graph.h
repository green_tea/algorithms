//
// Created by root on 22.10.18.
//

#ifndef ALGO_GRAPH_H
#define ALGO_GRAPH_H

#include <vector>
#include "Edge.h"

template <class T>
class Graph {
public:
    Graph(size_t size);

    Graph&operator=(const Graph& rhs);

    ~Graph();

    bool is_used(size_t vertex_num);

    virtual bool add_edge(const Edge& edge) = 0;

private:
    size_t size;
    std::vector<std::vector<size_t > > graph;
    bool* used;
};


#endif //ALGO_GRAPH_H
