//
// Created by root on 22.10.18.
//

#include "Edge.h"

Edge::Edge(size_t from, size_t to) {
    this->from_number = from;
    this->to_number = to;
}

Edge &Edge::operator=(const Edge &edge) {
    Edge(edge.from_number, edge.to_number);
    return *this;
}

size_t Edge::from() {
    return from_number;
}

size_t Edge::to() {
    return to_number;
}
