//
// Created by ice_tea on 01.12.18.
//

#ifndef ALGO_HASHED_ARRAY_TREE_H
#define ALGO_HASHED_ARRAY_TREE_H

#include <stdexcept>

template <typename T>
class hashed_array_tree {
public:
    void push(T value);

    T&operator[](std::size_t index);

    void pop();

    T& top();

    std::size_t size();

    T& at(std::size_t index);

    ~hashed_array_tree();

    hashed_array_tree();

private:

    void rebuild();

    void go_to_next_block();

    void go_to_prev_block();

    std::size_t _cur_pow = 0;
    std::size_t _calcs = 0;

    T* _last = nullptr;

    std::size_t _size = 0;
    std::size_t _max_size = 0;
    std::size_t _cur_block_size = 0;
    std::size_t _cur_block = 0;

    T** _data = nullptr;



};

#include "hashed_array_tree.hpp"

#endif //ALGO_HASHED_ARRAY_TREE_H
