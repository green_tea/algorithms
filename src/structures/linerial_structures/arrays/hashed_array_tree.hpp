//
// Created by ice_tea on 01.12.18.
//

#include "hashed_array_tree.h"

template<typename T>
void hashed_array_tree<T>::push(T value) {
    if (_size == _max_size)
        rebuild();
    if (_cur_block_size == _calcs)
        go_to_next_block();
    else {
        if (_last == nullptr) _last = &_data[0][0];
        else ++_last;
    }
    ++_cur_block_size;
    *_last = value;
    ++_size;
}

template<typename T>
T &hashed_array_tree<T>::operator[](std::size_t index) {

    return _data[index >> _cur_pow][index & (_calcs - 1)];
}
template<typename T>
void hashed_array_tree<T>::pop() {
    if (_cur_block_size == 0) throw std::invalid_argument("array is empty");
    --_last;
    --_cur_block_size;
    if (_cur_block_size == 0)
        if (_cur_block != 0) go_to_prev_block();
    --_size;
}

template<typename T>
T &hashed_array_tree<T>::top() {
    if (_size == 0) throw std::invalid_argument("array is empty");
    return *_last;
}

template<typename T>
std::size_t hashed_array_tree<T>::size() {
    return _size;
}

template<typename T>
void hashed_array_tree<T>::rebuild() {

    ++_cur_pow;
    std::size_t last_calcs = _calcs;
    _calcs = (1u << _cur_pow);

    T** new_data = new T*[_calcs];

    std::size_t cur_block_in_data = 0;

    std::size_t i;
    for (i = 0; cur_block_in_data < last_calcs; ++i) {

        new_data[i] = new T[_calcs];

        for (int j = 0; j < last_calcs; ++j) {
            new_data[i][j] = _data[cur_block_in_data][j];
        }

        delete[] _data[cur_block_in_data];
        _data[cur_block_in_data++] = nullptr;

        for (int j = 0; j < last_calcs; ++j) {
            new_data[i][j + last_calcs] = _data[cur_block_in_data][j];
        }

        delete[] _data[cur_block_in_data];
        _data[cur_block_in_data++] = nullptr;
    }

    if (i == 0) {
        new_data[0] = new T[_calcs];
        _cur_block_size = 0;
    }
    else _cur_block_size = _calcs;
    _max_size = _calcs * _calcs;
    _cur_block = i == 0 ? 0 : i - 1;

    delete[] _data;
    _data = new_data;

    if (_size == 0) _last = nullptr;
    else _last = &operator[](_size - 1);
}

template<typename T>
void hashed_array_tree<T>::go_to_next_block() {
    _data[++_cur_block] = new T[_calcs];
    _last = &_data[_cur_block][0];
    _cur_block_size = 0;
}

template<typename T>
void hashed_array_tree<T>::go_to_prev_block() {
    delete[] _data[_cur_block];
    _data[_cur_block--] = nullptr;
    _last = &_data[_cur_block][_calcs - 1];
    _cur_block_size = _calcs;
}

template<typename T>
T &hashed_array_tree<T>::at(std::size_t index) {
    if (index >= _size) throw std::invalid_argument("index is out of range");
    return operator[](index);
}

template<typename T>
hashed_array_tree<T>::~hashed_array_tree() {
    for (int i = 0; i <= _cur_block; ++i) {
        delete[] _data[_cur_block];
        _data[_cur_block] = nullptr;
    }
    delete _data;
    _data = nullptr;
}

template<typename T>
hashed_array_tree<T>::hashed_array_tree() {
    _data = new T*[1];
    _data[0] = new T[1];
}
