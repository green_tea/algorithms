//
// Created by ice_tea on 24.11.18.
//

//#include "persistent_stack.h"

template<typename T>
void persistent_stack<T>::push(T element, std::size_t version_number) {
    check_version(version_number);
    if (_cur_size == _cur_capacity) rebuild(_cur_capacity * 2);
    _data[_cur_size++] = std::make_pair(element, version_number);
}


template<typename T>
T persistent_stack<T>::pop(std::size_t version_number) {
    check_version(version_number);
    if (_data[version_number].second == STASH_NUMBER) throw std::invalid_argument("Этот стек пуст");
    T result = _data[version_number].first;
    element_type last_element = _data[_data[version_number].second];
    if (last_element.second == STASH_NUMBER) push_in_empty(last_element.first);
    else push(last_element.first, last_element.second);
    return result;
}

template<typename T>
void persistent_stack<T>::check_version(std::size_t version) {
    if (version >= _cur_size) throw std::invalid_argument("Не существует версии стека с номером" + version);
}

template<typename T>
persistent_stack<T>::persistent_stack() {
    _data[0] = std::make_pair(T(), STASH_NUMBER);
}

template<typename T>
T persistent_stack<T>::top(std::size_t version_number) {
    check_version(version_number);
    if (_data[version_number].second == STASH_NUMBER) throw std::invalid_argument("Этот стек пуст");
    return _data[version_number].first;
}

template<typename T>
void persistent_stack<T>::rebuild(std::size_t new_capacity) {
    element_type* new_data = new element_type[new_capacity];
    for (int i = 0; i < _cur_capacity; ++i) {
        new_data[i] = _data[i];
    }
    _cur_capacity *= 2;
    delete _data;
    _data = new_data;
}

template<typename T>
void persistent_stack<T>::push_in_empty(T element) {
    if (_cur_size == _cur_capacity) rebuild(_cur_capacity * 2);
    _data[_cur_size++] = std::make_pair(element, STASH_NUMBER);
}

template<typename T>
void persistent_stack<T>::push(T element) {
    push(element, _cur_size - 1);
}

template<typename T>
T persistent_stack<T>::pop() {
    pop(_cur_size - 1);
}

template<typename T>
T persistent_stack<T>::top() {
    top(_cur_size - 1);
}
