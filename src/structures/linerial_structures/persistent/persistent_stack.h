//
// Created by ice_tea on 24.11.18.
//

#ifndef ALGO_PERSISTENT_STACK_H
#define ALGO_PERSISTENT_STACK_H

#include <map> // for size_t and pair


template <typename T>
class persistent_stack {
public:

    persistent_stack();

    void push(T element, std::size_t version_number);

    T pop(std::size_t version_number);

    T top(std::size_t version_number);

    void push(T element);

    T pop();

    T top();

private:

    const std::size_t STASH_NUMBER = 10000000000; // число, означающее, что предыдущего элемента стека не существует

    typedef std::pair<T, std::size_t > element_type;

    element_type* _data = new element_type[1];

    std::size_t _cur_size = 1;

    std::size_t _cur_capacity = 1;

    void rebuild(std::size_t new_capacity);

    void check_version(std::size_t version);

    void push_in_empty(T element);
};

#include "persistent_stack.hpp"

#endif //ALGO_PERSISTENT_STACK_H
