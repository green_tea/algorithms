//
// Created by ice-tea on 07.09.18.
//

#include "Game_algo.h"

vector <int> Game_algo::retro_analyze(const graph &input_graph, const vector<int>& terminals) {
    graph reverse_graph(input_graph.size());
    for (int i = 0; i < input_graph.size(); ++i) {
        for (int to : input_graph[i]) {
            reverse_graph[to].push_back(i);
        }
    }
    vector <int> answers(input_graph.size());
    vector <int> degrees(input_graph.size());
    for (int i = 0; i < input_graph.size(); ++i) {
        degrees[i] = (int)input_graph[i].size();
    }
    std::fill(answers.begin(), answers.end(), -1);
    for (int terminal : terminals) {
        answers[terminal] = 0;
        dfs(terminal, reverse_graph, answers, degrees);
    }
    return answers;
}

// -1 - draw, 0 - lose, 1 - win,
void Game_algo::dfs(int vertex, const graph &input_graph, vector<int> &answers, vector <int> &degrees) {
    for (int to : input_graph[vertex]) {
        if (answers[to] != -1) continue;
        if (answers[vertex] == 0) {
            answers[to] = 1;
            dfs(to, input_graph, answers, degrees);
        }
        else {
            --degrees[to];
            if (degrees[to] == 0) {
                answers[to] = 0;
                dfs(to, input_graph, answers, degrees);
            }
        }
    }
}