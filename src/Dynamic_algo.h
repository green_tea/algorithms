//
// Created by ice-tea on 18.09.18.
//

#ifndef ALGO_DYNAMIC_ALGO_H
#define ALGO_DYNAMIC_ALGO_H

#include <vector>

class Dynamic_algo {
private:
    std::size_t bin_search(int value, std::size_t left, std::size_t right, const std::vector<int>& input_arr);

public:
    std::vector <int> largest_increasing_subsequence(const std::vector<int>& input_arr);

};


#endif //ALGO_DYNAMIC_ALGO_H
